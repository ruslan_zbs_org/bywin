var gulp           = require('gulp'),
		gutil          = require('gulp-util'),
		less           = require('gulp-less'),
		browserSync    = require('browser-sync'),
		concat         = require('gulp-concat'),
		uglify         = require('gulp-uglify-es').default,
		cleanCSS       = require('gulp-clean-css'),
		rename         = require('gulp-rename'),
		del            = require('del'),
		imagemin       = require('gulp-imagemin'),
		cache          = require('gulp-cache'),
		autoprefixer   = require('gulp-autoprefixer'),
		ftp            = require('vinyl-ftp'),
		notify         = require("gulp-notify"),
		rsync          = require('gulp-rsync'),
		rigger 		   = require('gulp-rigger');


	gulp.task('browser-sync', function() {
		browserSync({
			server: {
				baseDir: 'app'
			},
			notify: false,
			// tunnel: true,
			// tunnel: "projectmane", //Demonstration page: http://projectmane.localtunnel.me
		});
	});

gulp.task('js', function() {
	return gulp.src([
			'app/libs/jquery-3.3.1.min.js',
			'app/libs/bootstrap.min.js',
			'app/libs/jquery.barrating.min.js',
			'app/libs/owl.carousel.min.js',
			'app/libs/owl.carousel2.thumb.min',
			'app/js/main.js',
			'app/js/header.js'
		])
	.pipe(concat('scripts.min.js'))
	// .pipe(uglify()) // Минимизировать весь js (на выбор)
	.pipe(gulp.dest('app/js'))
	.pipe(browserSync.reload({ stream: true }));
});

gulp.task('less', function() {
	return gulp.src('app/less/*.less')
	.pipe(less({outputStyle: 'expanded'}).on("error", notify.onError()))
	.pipe(rename({suffix: '.min', prefix : ''}))
	.pipe(autoprefixer(['last 15 versions']))
	.pipe(cleanCSS()) // Опционально, закомментировать при отладке
	.pipe(gulp.dest('app/css'))
	.pipe(browserSync.stream())
});

gulp.task('html', function() {
    return gulp.src('app/template/*.html')
        .pipe(rigger()) //проходим через rigger
        .pipe(gulp.dest('app/'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('watch', ['less', 'js', 'html', 'browser-sync'], function() {
	gulp.watch('app/less/**/*.less', ['less']);
	gulp.watch('app/template/**/*.html', ['html']);
	gulp.watch(['libs/**/*.js', 'app/js/main.js'], ['js']);
	gulp.watch('app/*.html', browserSync.reload);
});

gulp.task('imagemin', function() {
	return gulp.src('app/images/**/*')
	.pipe(cache(imagemin())) // Cache Images
	.pipe(gulp.dest('dist/images')); 
});

gulp.task('build', ['imagemin', 'less', 'js', 'html'], function() {

	var buildFiles = gulp.src([
		'app/*.html',
		'app/.htaccess',
		'app/robots.txt',
		]).pipe(gulp.dest('dist/'));

	var buildCss = gulp.src([
		'app/css/style.min.css',
		]).pipe(gulp.dest('dist/css'));

	var buildJs = gulp.src([
		'app/js/scripts.min.js',
		'app/js/lazyload.min.js',
		'app/js/lazysizes.min.js',
		]).pipe(gulp.dest('dist/js'));

	var buildLibs = gulp.src([
		'app/libs/jquery-3.3.1.min.js',
		]).pipe(gulp.dest('dist/libs'));

	var buildFonts = gulp.src([
		'app/fonts/**/*',
		]).pipe(gulp.dest('dist/fonts'));

});

gulp.task('deploy', function() {

	var conn = ftp.create({
		host:      'test',
		user:      'test',
		password:  'test',
		parallel:  2,
		log: gutil.log
	});

	var globs = [
	'dist/**',
	'dist/.htaccess',
	];
	return gulp.src(globs, {buffer: false})
	.pipe(conn.dest('test'));

});

gulp.task('rsync', function() {
	return gulp.src('dist/**')
	.pipe(rsync({
		root: 'dist/',
		hostname: 'username@yousite.com',
		destination: 'yousite/public_html/',
		// include: ['*.htaccess'], // Скрытые файлы, которые необходимо включить в деплой
		recursive: true,
		archive: true,
		silent: false,
		compress: true
	}));
});

gulp.task('removedist', function() { return del.sync('dist'); });
gulp.task('clearcache', function () { return cache.clearAll(); });

gulp.task('default', ['watch']);
