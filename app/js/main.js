$(function () {
    $('.owl-carousel.owl-sliader').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        items: 1,
        navText: ['<i class="fas fa-chevron-circle-left"></i>', '<i class="fas fa-chevron-circle-right"></i>']
    })
    
    $(".heart").on("click", function () {
        if ($(this).children().hasClass('far')) {
            $(this).children().removeClass('far')
            $(this).children().addClass('fas')
        } else{
            $(this).children().removeClass('fas')
            $(this).children().addClass('far')
        }
    });
    
    $(".dec, .inc").on("click", function () {
        event.preventDefault();
        var $button = $(this);
        var oldValue = $button.parent().find("input").val();
        if ($button.text() === "+") {
            var newVal = parseFloat(oldValue) + 1;
        }
        else {
            if (oldValue > 1) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 1;
            }
        }
        $button.parent().find("input").val(newVal); // If you want to add a callback
    });

    // var flag = false;
    // if (document.documentElement.clientWidth < 991.5) {
    //     $('.nav-item .nav-link').each(function () {
    //         if ($(this).hasClass('active')) {
    //             if (flag === false) {
    //                 $('<a class="tab_menu_sub" href="#!"><i class="fas fa-chevron-circle-down"></i></a>').appendTo($(this));
    //                 flag = true;
    //             }
    //         } else {
    //             $(this).parent().appendTo($('.mobile_tab_link'));
    //         }
    //     })

    // } else {
    //     $('.mobile_tab_link .nav-item .nav-link').each(function () {
    //         console.log($(this));
    //         $(this).parent().appendTo($('.nav-pills'));
    //         $('.nav-item .nav-link.active').children().remove();
    //     })
    //     flag = false;
    // }
    // $(window).resize(function () {
    //     if (document.documentElement.clientWidth < 991.5) {
    //         $('.nav-item .nav-link').each(function () {
    //             if ($(this).hasClass('active')) {
    //                 if (flag === false) {
    //                 $('<a class="tab_menu_sub" href="#!"><i class="fas fa-chevron-circle-down"></i></a>').appendTo($(this));
    //                 flag = true;
    //                 }
    //         } else {
    //             return;
    //             }
    //         })

    //     } else {
    //         $('.mobile_tab_link .nav-item .nav-link').each(function () {
    //             console.log($(this));
    //             $(this).parent().appendTo($('.nav-pills'));
    //             $('.nav-item .nav-link.active').children().remove();
    //         })
    //         flag = false;
    //     }

                
    // })

    // $(".tab_menu_sub").on("click", function () {
    //     console.log(1);
    //     $('.nav-link').slideToggle();
    // });

    $('.bywin_product .owl-carousel.bywin_product_carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        navText: ['<i class="fas fa-chevron-circle-left"></i>', '<i class="fas fa-chevron-circle-right"></i>'],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    })
    $('.owl-carousel.we_support_carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        navText: ['<i class="fas fa-chevron-circle-left"></i>', '<i class="fas fa-chevron-circle-right"></i>'],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 2
            }
        }
    })
    

    var check = 1;
    var target = $('.we_count_anim_b');
    
    var targetPos = target.offset().top;
    var winHeight = $(window).height();
    var scrollToElem = targetPos - winHeight;

    $(window).scroll(function () {
        var winScrollTop = $(this).scrollTop();
        if (winScrollTop > scrollToElem && check) {
            $('.we_count_anim span').each(function () {
                $(this).prop('Counter', -1).animate({
                    Counter: $(this).text()
                }, {
                    duration: 1000,
                    easing: 'swing',
                    step: function (now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });
            check = 0;
        }
    });
    var check2 = 1;
    var target2 = $('.we_count_anim_b2');
    
    var targetPos2 = target2.offset().top;
    var winHeight2 = $(window).height();
    var scrollToElem2 = targetPos2 - winHeight2;

    $(window).scroll(function () {
        var winScrollTop2 = $(this).scrollTop();
        if (winScrollTop2 > scrollToElem2 && check2) {
            $('.we_count_anim span').each(function () {
                $(this).prop('Counter', -1).animate({
                    Counter: $(this).text()
                }, {
                    duration: 1000,
                    easing: 'swing',
                    step: function (now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });
            check2 = 0;
        }
    });
    
    var second_block = $('.second_block');
    second_block.each(function () {
        var sales = $(this).find('.second_block_text_green');
        var total_sales = $(this).find('.second_block_text_red');
        var calc = parseInt(59 - ((sales.attr('sales') / total_sales.attr('total_sales') * 100) / (100 / 59)))
        $(this).find('svg rect').attr('y', calc);
    })
});
