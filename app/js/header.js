$(document).ready(function () {
    var $sub_menu = $('.sub_menu');
    var $header_burger_icon = $('.header_burger_icon');
    var $nav_bar = $('.nav_bar');
    var $nav_bar_ul = $('.nav_bar>ul');
    var $how_it_works = $('.how_it_works');
    var $logo = $('.logo');
    var $header = $('.header .container .row');
    var $header_contact = $('.header_contact');
    var $header_registration = $('.header_registration');
    var $header_wishlist = $('.header_wishlist');
    var $header_cart = $('.header_cart');
    var $header_language = $('.header_language');
    var $header_burger = $('.header_burger');
    var $menu_mobile = $('<div class="menu_mobile">');
    var $mobile_link = $('<div class="menu_mobile_link">');
    var $header_language_ul = $('.header_language ul');
    $header_language_ul.hide();
    $sub_menu.click(function (e) {
        e.preventDefault();
        $(this).find('ul').slideToggle();
    });

    $header_burger_icon.click(function (e) {
        e.preventDefault();
        $(this).toggleClass('fa-bars');
        $(this).toggleClass('active');
        $(this).toggleClass('fa-times');
        $nav_bar.slideToggle();
    });

    function howItWorks(burger, nav_bar_ul, mobile_link, mobile, data, el, es, header, contact, registration, wishlist, cart, language) {
        if (document.documentElement.clientWidth > 1199.5) {
            el.show();
        }
        if (document.documentElement.clientWidth <= 992) {
            data.prependTo(el);
        } else {
            data.prependTo(header);
            es.prependTo(header);
        }
        if (document.documentElement.clientWidth <= 767.5) {
            mobile.appendTo(el);
            mobile_link.prependTo(el);
            data.prependTo(mobile_link);
            nav_bar_ul.appendTo(mobile_link);
            contact.appendTo(mobile);
            registration.appendTo(mobile);
            wishlist.appendTo(mobile);
            cart.appendTo(mobile);
            language.appendTo(mobile);
        }
        if (document.documentElement.clientWidth >= 767.5) {
            language.prependTo(header);
            cart.prependTo(header);
            wishlist.prependTo(header);
            registration.prependTo(header);
            contact.prependTo(header);
            el.prependTo(header);
            // burger.prependTo(header);
            // data.prependTo(header);
            es.prependTo(header);
        }
        if (document.documentElement.clientWidth >= 991.5) {
            language.prependTo(header);
            cart.prependTo(header);
            wishlist.prependTo(header);
            registration.prependTo(header);
            contact.prependTo(header);
            el.prependTo(header);
            // burger.prependTo(header);
            data.prependTo(header);
            es.prependTo(header);
        }
    }
    howItWorks($header_burger, $nav_bar_ul, $mobile_link, $menu_mobile, $how_it_works, $nav_bar, $logo, $header, $header_contact, $header_registration, $header_wishlist, $header_cart, $header_language);
    $(window).resize(function () {
        howItWorks($header_burger, $nav_bar_ul, $mobile_link, $menu_mobile, $how_it_works, $nav_bar, $logo, $header, $header_contact, $header_registration, $header_wishlist, $header_cart, $header_language);
    })
});